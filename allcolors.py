#!/usr/bin/env python

import colorsys
import signal
import time

from sys import exit

import unicornhathd




unicornhathd.rotation(0)
unicornhathd.brightness(1.0)


width, height = unicornhathd.get_shape()

ss=0;
for s in range(256*256*256):
    for x in range(width):
        for y in range(height):
            ss=ss+1
            unicornhathd.set_pixel(x, y, ss%256, (ss//256)%256, (ss//(256*256))%256)

        unicornhathd.show()
        #time.sleep(0.01)

unicornhathd.off()
