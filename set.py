#!/usr/bin/env python

import colorsys
import signal
import time

from sys import exit

import unicornhathd


unicornhathd.brightness(1.0)


width, height = unicornhathd.get_shape()

unicornhathd.set_all(0,0,255)

unicornhathd.show()

